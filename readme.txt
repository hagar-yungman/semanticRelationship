ReadMe:
the goal of the program:
	cluster pairs of niminal according  to the following semantic relations -
	COORD: the first noun is a co-hyponym (coordinate) of the second noun, i.e., they belong to the same semantic class: alligator-coord-lizard.
	HYPER: the first noun is a hypernym of the second noun: alligator-hyper-animal.
	MERO: the first noun refers to a part/component/organ/member of the second noun, or something that the second noun contains or is made of: alligator-mero-mouth.
	RANDOM: the two nouns are not semantically related.
	this is an unsupervised multi-class classification according to the paper of Dmitry Davidov and Ari Rappoport: Classification of Semantic Relationships between Nominals Using Pattern Clusters

in general:
selecting hook words:
1. select all distinct words appeared in the training (___) to get patterns that will be relevant to the problem
2. added ____ nouns from 1gram Google corpus. preformed a word count with M-R, ordered the result in decending order
then filtered the nouns.
prepare Patterns:
1.upload hook words to S3
2.preform M-R on Google 5gram corpus. 
	uploaded hook words from S3 to memory.
	mapper filter sentences where the word in the second place or forth place are from the selected hook word. and sends to the reducer key - hook target value - sentence.
	reducer - receives key of hook target and concatenate to it all distinct sentences found.
	the results are saved on S3.
3.download M-R results from S3 and load it to a map.
filter Patterns:
1.for each hook word combine targets if 2/3 of their patters are similar (the check is according to the smaller pattern list)
2.delete all targets thats from a single hook word
3.combine targets from different hook words where 2/3 of their patters are similar, the similar patterns when combining are marked 
as core, the unique ones marked as unconfirmed
create clusters:
1.from the filtered patterns 
handle training data:
1.upload training data from S3 to map of pairs
2.for each pair calculate hits to every cluster
3.save each pair - hits vector to file
4.use Weka to classify the file.

classes and functions
CountWords.java - M-R class to count words
GetPatterns.java - M-R class to create the patterns
	map(LongWritable key, Text value, Context context) - 
		get sentence from 5 gram corpus and checks if the second or forth word is hook word, if so it passes the hook and the target as key and the sentence as the value.
	reduce(Text key, Iterable<Text> values, Context context) - 
		concatenate all sentences to the hook and target. assumption on the memory: all sentences for hook and target can be saved in memory
Runner.java	- main for M-R steps.
CompareHookToCorpus.java - checks if in the selected hook list there are words not in the corpus - so that the sentences will match the training data.
	downloadFilesFromS3(String filepath, HashMap<String, String> saveMap)
		list the files in path filepath and calls insert to map to save the files to map
	insertToMap(S3Object file, HashMap<String, String> saveMap)
		get file download it and save to map
Pair<R,L>.java - pair object with left and right generic fields.
Pattern.java - object saving the pattern, all hook and target pairs that has this pattern and if the pattern is core
Clusters.java - prepare the clusters from the patterns and prepare training with hits measure to each cluster
	createPatternsFromS3(String filepath) + insertToMap(S3Object file) - 
		prepare the structure of all patterns
	groupClustersForHook() - 
		for each hook word, group targets with at least 2/3 of similar patterns.
	addPatternsToTarget(String hook, String fromTarget, HashSet<Pattern> fromPatterns, HashSet<Pattern> toPatterns) 
		add toPatterns all patterns from fromPatterns. for all the similar patterns it ads the pairList from fromPatterns to toPatterns
	compare2targetPatterns(HashSet<Pattern> pattern1, HashSet<Pattern> pattern2)
		compare pattern1 to pattern2, going over the smaller list. returns the ratio of similar patterns
	deletePatternsFromSingleHook()
		delete all patterns that appear only on one hook
	groupClusters()
		group targets from different hook words with with at least 2/3 of similar patterns. (according to the algorithm in the article)
	isTargetPatternsCore(HashSet<Pattern> patterns)
		returns if the are core patterns in the list
	filterHookPatterns()
		loop over all hook words and delete hooks with no targets
	getClusters()
		convert patters structure to cluster structure
	getHits()
		for each pair in training create hits vector
	removeZerosVector(HashMap<Pair<String, String>, ArrayList<Double>> hits)
		remove all pairs from training with hits vector that contains only zeros
	writeToFile(HashMap<Pair<String, String>, ArrayList<Double>> hits, File train, File record)
		write hits vector to file in s3 and write to another file the hits vector with the original pair
	arrayListToString(ArrayList<Double> list)
		returns string with all values of the list concatenate with ','
	uploadFileToS3(File file, String path)
		upload file to path in s3
	insertPairsToList(String fileName)
		reads training pairs and add to list
		
working with Weka:

Map input records=3839
Map output records=2896

Reduce input records=2896
Reduce output records=301

incorrect classification:
box paper mero -> random
car transport hyper -> random

correct classification:
saw conduct random
bear force random
train accompany random
