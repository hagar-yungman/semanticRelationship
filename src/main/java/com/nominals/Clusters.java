package com.nominals;

import com.amazonaws.AmazonClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;

import java.io.*;
import java.util.*;

public class Clusters {
    public final static Regions REGION = Regions.US_EAST_1;
    static long FH = 301888 * 100;
    private static HashMap<String, String> oneGramWordsMap = new HashMap<>();
    private static HashMap<Pair<String, String>, String> twoGramWordsMap = new HashMap<>();
    private static HashMap<String, HashMap<String, HashSet<Pattern>>> newHookPatterns = new HashMap<>();
    private static String wordCountOutput = "nominals/wordCount1-output/part-r-";
    private static String twoGramWordCountOutput = "nominals/count2/part-r-";
    private static String bucketName = "hagarnoadsp";
    private static String patternOutput = "nominals/new-patterns-output6/part-r-";
    private static HashMap<String, HashMap<String, HashSet<Pattern>>> hookPatterns = new HashMap<>();
    private static ArrayList<Pattern> clusters;
    private static HashMap<Pair<String, String>, String> pairTag = new HashMap<>();
    private static AmazonS3 S3 = AmazonS3ClientBuilder.standard()
            .withRegion(REGION)
            .build();

    public static void main(String[] args) {
        downloadFilesFromS3(wordCountOutput);
        File file = new File("part-r-00000.txt");
        insertToMap(file);
        createPatternsFromS3(patternOutput);
        removeHookWithNoPatterns();
        filterByPmi();
        groupClustersForHook();
        deletePatternsFromSingleHook();
        groupClusters();
        getHits();

    }

    private static void insertToMap(File file) {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                line = line.toString().replace("\t", " ");
                String[] splitWordsBySpace = line.split(" ");
                if ((splitWordsBySpace[0].matches("[A-Za-z]+")) && (splitWordsBySpace[1].matches("[A-Za-z]+"))) {
                    twoGramWordsMap.put(new Pair(splitWordsBySpace[0], splitWordsBySpace[1]), splitWordsBySpace[2]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createPatternsFromS3(String filepath) {
        S3Object file = null;
        System.out.println("Listing objects");
        final ListObjectsV2Request req = new ListObjectsV2Request()
                .withBucketName(bucketName)
                .withPrefix(filepath);
        ListObjectsV2Result result = S3.listObjectsV2(req);
        for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
            try {
                file = S3.getObject(new GetObjectRequest(bucketName, objectSummary.getKey()));
                insertToMap(file);
            } catch (AmazonClientException e) {
                e.printStackTrace();
            }
        }
    }

    private static void insertToMap(S3Object file) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getObjectContent()));
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                String[] splitHookTargetPatterns = line.split("\t");
                String[] hookTarget = splitHookTargetPatterns[0].split(" ");
                if (!hookPatterns.containsKey(hookTarget[0])) {
                    hookPatterns.put(hookTarget[0], new HashMap<String, HashSet<Pattern>>());
                }
                HashSet<Pattern> sentencesPatterns = new HashSet<>();
                boolean insertToMap = false;
                for (int i = 1; i < splitHookTargetPatterns.length; i++) {
                    String[] words = splitHookTargetPatterns[i].split(" ");
                    for (int k = 0; k < 5; k = k + 2) {
                        if (oneGramWordsMap.containsKey(words[k])) {
                            long numOfOcurance = Long.parseLong(oneGramWordsMap.get(words[k]), 10);
                            if (numOfOcurance >= FH) {
                                insertToMap = true;
                            } else {
                                insertToMap = false;
                            }
                        }
                    }
                    String pattern = String.format("%s %s %s", words[0], words[2], words[4]);
                    sentencesPatterns.add(new Pattern(new Pair<String, String>(hookTarget[0], hookTarget[1]), pattern));
                }
                if (insertToMap) {
                    hookPatterns.get(hookTarget[0]).put(hookTarget[1], sentencesPatterns);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void removeHookWithNoPatterns() {
        for (Map.Entry<String, HashMap<String, HashSet<Pattern>>> HookTargetsPatterns : hookPatterns.entrySet()) {
            if (HookTargetsPatterns.getValue().size() != 0) {
                newHookPatterns.put(HookTargetsPatterns.getKey(), HookTargetsPatterns.getValue());
            }
        }
    }

    private static void filterByPmi() {
        for (Map.Entry<String, HashMap<String, HashSet<Pattern>>> HookTargetsPatterns : hookPatterns.entrySet()) {
            TreeMap<Double, Pair<String, String>> pmis = new TreeMap<>();
            TreeMap<Double, Pair<String, String>> negPmis = new TreeMap<>();
            if (HookTargetsPatterns.getValue().size() >= 5) {
                double size = HookTargetsPatterns.getValue().size();
                for (Map.Entry<String, HashSet<Pattern>> targetPatterns : new HashMap<>(HookTargetsPatterns.getValue()).entrySet()) {
                    Pair hookTarget = new Pair(HookTargetsPatterns.getKey(), targetPatterns.getKey());
                    double hookCount = Math.log10(Double.parseDouble(oneGramWordsMap.get(hookTarget.getLeft())));
                    double hookTargetPair = 0;
                    double targetCount = Math.log10(Double.parseDouble(oneGramWordsMap.get(hookTarget.getRight())));
                    if (twoGramWordsMap.containsKey(hookTarget)) {
                        hookTargetPair = Math.log10(Double.parseDouble(twoGramWordsMap.get(hookTarget)));
                    }
                        double oneGramCount = Math.log10(301888295413L);
                        double pmi = hookTargetPair + oneGramCount - hookCount - targetCount;
                        pmis.put(pmi, hookTarget);
                }
                int numOfPatternsToDelete = (int) (size * 0.2);
                int count = 0;
                for (Pair<String, String> pair : pmis.values()) {
                    newHookPatterns.get(pair.getLeft()).remove(pair.getRight());
                    count++;
                    if (count == numOfPatternsToDelete) {
                        break;
                    }
                }
                for (double pmi : pmis.keySet()) {
                    negPmis.put(-pmi, pmis.get(pmi));
                }
                count = 0;
                for (Pair<String, String> pair : negPmis.values()) {
                    newHookPatterns.get(pair.getLeft()).remove(pair.getRight());
                    count++;
                    if (count == numOfPatternsToDelete) {
                        break;
                    }
                }
            }
        }
    }

    //for each hook, for each target, we merge similar hookPatterns
    private static void groupClustersForHook() {
        for (Map.Entry<String, HashMap<String, HashSet<Pattern>>> HookTargetsPatterns : newHookPatterns.entrySet()) {
            for (Map.Entry<String, HashSet<Pattern>> targetPatterns : new HashMap<>(HookTargetsPatterns.getValue()).entrySet()) {
                boolean toErase = false;
                for (Map.Entry<String, HashSet<Pattern>> validatePatterns : new HashMap<>(HookTargetsPatterns.getValue()).entrySet()) {
                    if (!targetPatterns.getKey().equals(validatePatterns.getKey())) {
                        //check 2 target patterns
                        double similarity = compare2targetPatterns(targetPatterns.getValue(), validatePatterns.getValue());
                        if (similarity >= 0.66) {
                            addPatternsToTarget(HookTargetsPatterns.getKey(), targetPatterns.getKey(), targetPatterns.getValue(), validatePatterns.getValue());
                            toErase = true;
                        }
                    }
                }
                if (toErase) {
                    HookTargetsPatterns.getValue().remove(targetPatterns.getKey());
                }
            }
        }
    }

    private static void addPatternsToTarget(String hook, String fromTarget, HashSet<Pattern> fromPatterns, HashSet<Pattern> toPatterns) {
        HashSet<Pattern> addList = new HashSet<>();
        for (Pattern fromPattern : fromPatterns) {
            boolean contain = false;
            for (Pattern toPattern : toPatterns) {
                if (toPattern.equals(fromPattern)) {
                    toPattern.addPairsToPattern(fromPattern.getPairList());
                    contain = true;
                    break;
                }
            }
            if (!contain) {
                addList.add(fromPattern);
            }
        }
        toPatterns.addAll(addList);
    }

    private static double compare2targetPatterns(HashSet<Pattern> pattern1, HashSet<Pattern> pattern2) {
        int counter = 0;
        int size = 0;
        if (pattern1.size() <= pattern2.size()) {
            size = pattern1.size();
            for (Pattern pattern : pattern1) {
                for (Pattern checkPattern : pattern2) {
                    if (checkPattern.equals(pattern)) {
                        counter++;
                    }
                }
            }
        } else {
            size = pattern2.size();
            for (Pattern pattern : pattern2) {
                for (Pattern checkPattern : pattern1) {
                    if (checkPattern.equals(pattern)) {
                        counter++;
                    }
                }
            }
        }
        return (double) counter / (double) size;
    }

    private static void deletePatternsFromSingleHook() {
        HashMap<String, Integer> allPatterns = new HashMap<>();
        //list all patterns
        for (HashMap<String, HashSet<Pattern>> targetsPatterns : newHookPatterns.values()) {
            for (HashSet<Pattern> patterns : targetsPatterns.values()) {
                for (Pattern pattern : patterns) {
                    int count = 1;
                    if (allPatterns.containsKey(pattern.getPatt())) {
                        count = allPatterns.get(pattern.getPatt()) + 1;
                    }
                    allPatterns.put(pattern.getPatt(), count);
                }
            }
        }
        //filter patterns from one hook
        for (Map.Entry<String, Integer> patternStr : allPatterns.entrySet()) {
            if (patternStr.getValue() == 1) {
                boolean isFound = false;
                for (Map.Entry<String, HashMap<String, HashSet<Pattern>>> hookTargetsPatterns : newHookPatterns.entrySet()) {
                    for (Map.Entry<String, HashSet<Pattern>> targetPatterns : (hookTargetsPatterns.getValue()).entrySet()) {
                        for (Pattern pattern : new HashSet<Pattern>(targetPatterns.getValue())) {
                            if (patternStr.getKey().equals(pattern.getPatt())) {
                                isFound = true;
                                targetPatterns.getValue().remove(pattern);
                                break;
                            }
                        }
                        if (targetPatterns.getValue().size() == 0) {
                            hookTargetsPatterns.getValue().remove(targetPatterns.getKey());
                        }
                        if (isFound) break;
                    }
                    if (isFound) break;
                }
            }
        }
        filterHookPatterns();
    }

    private static void groupClusters() {
        while (true) {
            int minSize = 1000;
            String minTarget = "";
            String minHook = "";
            boolean isUnconfirmd = false;
            for (Map.Entry<String, HashMap<String, HashSet<Pattern>>> hookTargetsPatterns : newHookPatterns.entrySet()) {
                for (Map.Entry<String, HashSet<Pattern>> targetPatterns : new HashMap<String, HashSet<Pattern>>(hookTargetsPatterns.getValue()).entrySet()) {
                    boolean isCore = isTargetPatternsCore(targetPatterns.getValue());
                    if ((!isCore) && (targetPatterns.getValue().size() < minSize)) {
                        minSize = targetPatterns.getValue().size();
                        minTarget = targetPatterns.getKey();
                        minHook = hookTargetsPatterns.getKey();
                        isUnconfirmd = true;
                    }
                }
            }
            if (!isUnconfirmd) {
                filterHookPatterns();
                return;
            }
            HashSet<Pattern> C1 = newHookPatterns.get(minHook).get(minTarget);
            boolean deleteC1 = true;
            HashSet<Pattern> toAdd = new HashSet<>();
            for (Map.Entry<String, HashMap<String, HashSet<Pattern>>> hookTargetsPatterns : newHookPatterns.entrySet()) {
                if (minHook != hookTargetsPatterns.getKey()) {
                    HashSet<String> toErase = new HashSet<>();
                    for (Map.Entry<String, HashSet<Pattern>> targetPatterns : new HashMap<String, HashSet<Pattern>>(hookTargetsPatterns.getValue()).entrySet()) {
                        double similarity = compare2targetPatterns(targetPatterns.getValue(), C1);
                        if (similarity >= 0.66) {
                            deleteC1 = false;
                            for (Pattern pattern : targetPatterns.getValue()) {
                                boolean isSimilar = false;
                                for (Pattern c1Pattern : C1) {
                                    if (pattern.equals(c1Pattern)) {
                                        c1Pattern.addPairsToPattern(pattern.getPairList());
                                        c1Pattern.setCore(true);
                                        isSimilar = true;
                                        break;
                                    }
                                }
                                if (!isSimilar) {
                                    toAdd.add(new Pattern(pattern));
                                }
                            }
                            toErase.add(targetPatterns.getKey());
                        }
                    }
                    if (toErase.size() > 0) {
                        for (String eraseKey : toErase) {
                            hookTargetsPatterns.getValue().remove(eraseKey);
                        }
                    }
                }
            }
            if (toAdd.size() > 0) {
                C1.addAll(toAdd);
                minSize++;
            }
            if (deleteC1) {
                newHookPatterns.get(minHook).remove(minTarget);
            }
        }

    }

    private static boolean isTargetPatternsCore(HashSet<Pattern> patterns) {
        boolean isCore = false;
        for (Pattern pattern : patterns) {
            if (pattern.isCore()) {
                isCore = true;
                break;
            }
        }
        return isCore;
    }

    private static void filterHookPatterns() {
        for (Map.Entry<String, HashMap<String, HashSet<Pattern>>> hookTargetsPatterns : new HashMap<String, HashMap<String, HashSet<Pattern>>>(newHookPatterns).entrySet()) {
            if (hookTargetsPatterns.getValue().size() == 0) {
                newHookPatterns.remove(hookTargetsPatterns.getKey());
            }
        }

    }

    private static ArrayList<ArrayList<Pattern>> getClusters() {
        ArrayList<ArrayList<Pattern>> clusters = new ArrayList<>();
        for (Map.Entry<String, HashMap<String, HashSet<Pattern>>> patternsForHookTarget : newHookPatterns.entrySet()) {
            for (Map.Entry<String, HashSet<Pattern>> targetPatterns : patternsForHookTarget.getValue().entrySet()) {
                ArrayList<Pattern> patternList = new ArrayList<>();
                for (Pattern pattern : targetPatterns.getValue()) {
                    patternList.add(pattern);
                }
                clusters.add(patternList);
            }
        }
        return clusters;
    }

    private static void getHits() {
        File train = new File("trainSet6.txt");
        File allData = new File("allDataPerPair6.txt");
        try {
            train.createNewFile();
            allData.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<ArrayList<Pattern>> clusters = getClusters();
        HashSet<Pair<String, String>> pairs = insertPairsToList("BLESS");
        double alpha = 0.01;
        HashMap<Pair<String, String>, ArrayList<Double>> hits = new HashMap<>();
        for (ArrayList<Pattern> cluster : clusters) {
            int coreSize = 0;
            int unconfirmedSize = 0;
            for (Pattern pattern : cluster) {
                if (pattern.isCore()) coreSize++;
                else unconfirmedSize++;
            }
            for (Pair pair : pairs) {
                int coreTimes = 0;
                int unconfirmedTimes = 0;
                for (Pattern pattern : cluster) {
                    if (pattern.isCore()) {
                        if (pattern.getPairList().contains(pair)) {
                            coreTimes++;
                        }
                    } else {
                        if (pattern.getPairList().contains(pair)) {
                            unconfirmedTimes++;
                        }
                    }
                }
                double match = 0;
                if (unconfirmedSize == 0) {
                    match = ((double) coreTimes / (double) coreSize);
                } else {
                    match = ((double) coreTimes / (double) coreSize) + (alpha * ((double) unconfirmedTimes / (double) unconfirmedSize));
                }
                if (!hits.containsKey(pair)) {
                    ArrayList hitsList = new ArrayList();
                    hitsList.add(match);
                    hits.put(pair, hitsList);
                } else {
                    hits.get(pair).add(match);
                }
            }
        }
        removeZerosVector(hits);
        writeToFile(hits, train, allData);
    }

    private static void removeZerosVector(HashMap<Pair<String, String>, ArrayList<Double>> hits) {
        boolean dontErase = false;
        ArrayList<Pair<String, String>> toErase = new ArrayList<>();
        for (Map.Entry<Pair<String, String>, ArrayList<Double>> pairVector : hits.entrySet()) {
            for (Double feature : pairVector.getValue()) {
                if (feature != 0.0) {
                    dontErase = true;
                }
            }
            if (!dontErase) {
                toErase.add(pairVector.getKey());
            }
            dontErase = false;
        }
        for (Pair toDelete : toErase) {
            hits.remove(toDelete);
        }
    }

    private static void writeToFile(HashMap<Pair<String, String>, ArrayList<Double>> hits, File train, File record) {
        for (Map.Entry<Pair<String, String>, ArrayList<Double>> pairVector : hits.entrySet()) {
            BufferedWriter bufferedWriter;
            String tag = pairTag.get(pairVector.getKey());
            String vector = arrayListToString(pairVector.getValue());
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(record, true));
                bufferedWriter.write(String.format("%s %s %s %s\n", pairVector.getKey().getLeft(), pairVector.getKey().getRight(), vector, tag));
                bufferedWriter.close();
                bufferedWriter = new BufferedWriter(new FileWriter(train, true));
                bufferedWriter.write(String.format("%s%s\n", vector, tag));
                bufferedWriter.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        uploadFileToS3(train, "nominals/newTrainingSet/");
        uploadFileToS3(record, "nominals/newTrainingSet/");

    }

    private static String arrayListToString(ArrayList<Double> list) {
        StringBuilder sb = new StringBuilder();
        for (Double d : list) {
            sb.append(d);
            sb.append(",");
        }
        return sb.toString();
    }

    private static void uploadFileToS3(File file, String path) {
        try {
            System.out.println("Uploading a new object to S3 from a file\n");
            S3.putObject(new PutObjectRequest(
                    bucketName, path + file.getName(), file));
            file.delete();
        } catch (AmazonClientException e) {
            e.printStackTrace();
        }
    }

    private static HashSet<Pair<String, String>> insertPairsToList(String fileName) {
        HashSet<Pair<String, String>> pairs = new HashSet<>();
        S3Object file = null;
        final ListObjectsV2Request req = new ListObjectsV2Request()
                .withBucketName(bucketName)
                .withPrefix("nominals/TrainingSet/BLESS");
        ListObjectsV2Result result = S3.listObjectsV2(req);
        for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
            try {
                file = S3.getObject(new GetObjectRequest(bucketName, objectSummary.getKey()));
            } catch (AmazonClientException e) {
                e.printStackTrace();
            }
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getObjectContent()));
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                String[] training = line.split("\t");
                pairTag.put(new Pair(training[0], training[1]), training[2]);
                pairs.add(new Pair(training[0], training[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pairs;
    }

    private static void downloadFilesFromS3(String filepath) {
        S3Object file = null;
        System.out.println("Listing objects");
        final ListObjectsV2Request req = new ListObjectsV2Request()
                .withBucketName(bucketName)
                .withPrefix(filepath);
        ListObjectsV2Result result = S3.listObjectsV2(req);
        for (S3ObjectSummary objectSummary : result.getObjectSummaries()) {
            try {
                file = S3.getObject(new GetObjectRequest(bucketName, objectSummary.getKey()));
                insertWordsToMap(file);
            } catch (AmazonClientException e) {
                e.printStackTrace();
            }
        }
    }

    private static void insertWordsToMap(S3Object file) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getObjectContent()));
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                line = line.toString().replace("\t", " ");
                String[] splitWordsBySpace = line.split(" ");
                if (!oneGramWordsMap.containsKey(splitWordsBySpace[0])) {
                    oneGramWordsMap.put(splitWordsBySpace[0], splitWordsBySpace[1]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}




