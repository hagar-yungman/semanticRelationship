package com.nominals;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.fs.s3a.S3AFileSystem;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import java.io.*;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
//import java.util.regex.Pattern;

public class GetPatterns {
    public static class MapClass extends Mapper<LongWritable, Text, Text, Text> {
        private static Map<String, String> hookWords = new ConcurrentHashMap<String,String>();

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {

            URI[] cacheFilesUri = Job.getInstance(context.getConfiguration()).getCacheFiles();
            System.out.println("Pair print in  setup");
            FileSystem fileSystem = FileSystem.get(context.getConfiguration());
            for (URI cacheFileUri : cacheFilesUri) {
                loadHookWordsHashMap(cacheFileUri);
            }
        }
        private void loadHookWordsHashMap(URI cacheFile) throws IOException {
            String strLineRead;
            long FC = 301888 * 1000;
            long FB = 301888 * 50;
            BufferedReader brReader = null;
            try {
                brReader = new BufferedReader(new InputStreamReader(new FileInputStream
                        (new Path(cacheFile.getPath()).getName()), StandardCharsets.UTF_8));
                while ((strLineRead = brReader.readLine()) != null) {
                    String word[] = strLineRead.split("\t");
                    long numOfOcurance = Long.parseLong(word[1],10);
                    if ((numOfOcurance >= FB) && (numOfOcurance <= FC)) {
                        hookWords.put(word[0], word[1]);
                    }
                }
            } finally {
                if (brReader != null) {
                    brReader.close();
                }
            }
        }

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String ngram = value.toString().toLowerCase();
            String[] sentence = ngram.split(" ");
            if (sentence.length != 5) {
                System.err.println("ERROR : wrong number of words in value ( it's not 5). ");
                return;
            }
            for (String word : sentence) {
                if(!word.matches("[A-Za-z]+"))
                    return;
                if (word.equals(""))
                    return;
            }
            try {
                if (hookWords.containsKey(sentence[1])){
                    context.write(new Text(new Pair(sentence[1],sentence[3]).toString()), new Text(ngram));
                }

                if (hookWords.containsKey(sentence[3])) {
                    context.write(new Text(new Pair(sentence[3], sentence[1]).toString().toLowerCase()), new Text(ngram.toLowerCase()));
                }
            }
            catch (ArrayIndexOutOfBoundsException e){
                throw new IOException("the sentence : " + ngram);
            }
        }
    }

    public static class ReduceClass extends Reducer<Text, Text, Text, Text> {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            HashSet<String> targetPairList = new HashSet<>();
            StringBuilder result = new StringBuilder();
            for (Text value : values){
                targetPairList.add(value.toString());
            }
            for (String line: targetPairList){
                result.append(line);
                result.append("\t");
            }
            context.write(key, new Text(result.toString()));
        }
    }

    public static class PartitionerClass extends Partitioner<Text, Text> {
        @Override
        public int getPartition(Text key, Text value, int numPartitions) {
            if (key.toString().equals("numOfHooks")) return 0 ;
            return Math.abs(key.hashCode()) % numPartitions;
        }
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        Job job = new Job(conf, "targetTables");
        job.setJarByClass(GetPatterns.class);
        job.setMapperClass(MapClass.class);
        job.setPartitionerClass(PartitionerClass.class);
        job.setReducerClass(ReduceClass.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setInputFormatClass(TextInputFormat.class);
        FileSystem fileSystem = new S3AFileSystem();
        //args[2] = bucket name
        fileSystem.initialize(URI.create(args[2]), conf);
        //args[3] = path to hooks words
        RemoteIterator<LocatedFileStatus> itr = fileSystem.listFiles(new Path(args[3]), false);
        while (itr.hasNext()){
            LocatedFileStatus file = itr.next();
            if(file.getPath().getName().equals("_SUCCESS")){
                continue;
            }
            System.out.println("uri: " + file.getPath().toUri());
            System.out.println("path name: " + file.getPath().getName());
            job.addCacheFile(file.getPath().toUri());
        }
        fileSystem.close();
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
